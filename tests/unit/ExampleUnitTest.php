<?php
/**
 * Image Boxer plugin for Craft CMS 3.x
 *
 * Resize and optimise  images
 *
 * @link      https://hiller.digital
 * @copyright Copyright (c) 2021 Matthias Hiller
 */

namespace hillerdigital\imageboxertests\unit;

use Codeception\Test\Unit;
use UnitTester;
use Craft;
use hillerdigital\imageboxer\ImageBoxer;

/**
 * ExampleUnitTest
 *
 *
 * @author    Matthias Hiller
 * @package   ImageBoxer
 * @since     1.0.0
 */
class ExampleUnitTest extends Unit
{
    // Properties
    // =========================================================================

    /**
     * @var UnitTester
     */
    protected $tester;

    // Public methods
    // =========================================================================

    // Tests
    // =========================================================================

    /**
     *
     */
    public function testPluginInstance()
    {
        $this->assertInstanceOf(
            ImageBoxer::class,
            ImageBoxer::$plugin
        );
    }

    /**
     *
     */
    public function testCraftEdition()
    {
        Craft::$app->setEdition(Craft::Pro);

        $this->assertSame(
            Craft::Pro,
            Craft::$app->getEdition()
        );
    }
}
