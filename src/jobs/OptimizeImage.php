<?php
namespace hillerdigital\imageboxer\jobs;

use Craft;
use craft\queue\BaseJob;
use hillerdigital\imageboxer\ImageBoxer;

class OptimizeImage extends BaseJob
{

    public $model;

    public function execute($queue)
    {
        ImageBoxer::$plugin->imageBoxer->optimizeTransformedImages($this->model);
    }

    protected function defaultDescription(): string
    {
        return Craft::t('image-boxer', 'Optimize Image');
    }
}
