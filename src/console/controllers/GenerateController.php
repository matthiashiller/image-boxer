<?php


namespace hillerdigital\imageboxer\console\controllers;


use Craft;
use hillerdigital\imageboxer\ImageBoxer;
use hillerdigital\imageboxer\jobs\OptimizeImage;
use hillerdigital\imageboxer\models\TransformModel;
use hillerdigital\imageboxer\transformers\Transformer;
use yii\console\Controller;
use yii\console\ExitCode;
use \yii\helpers\Console;

class GenerateController extends Controller
{

    public function actionIndex()
    {
        $imagesToTransform = ImageBoxer::$plugin->imageBoxer->generators();
        $total = count($imagesToTransform);
        $count = 1;
        $this->stdout(count($imagesToTransform).' Images will be generated.' . PHP_EOL, Console::FG_PURPLE);
        foreach ($imagesToTransform as $transform) {
            /* @var TransformModel $transform */
            $asset = $transform->asset;
            $msg = "transforming {$count}/{$total} Asset: {$asset->id} ({$transform->width} / {$transform->height})".PHP_EOL;
            $this->stdout($msg, Console::FG_YELLOW);

            Transformer::transformForSite($transform, true);

            $transform->unsetImageInstance();
            $queue = Craft::$app->getQueue();
            $queue->push(new OptimizeImage([
                'model' => $transform,
            ]));


            $count ++;
        }

        $this->stdout('done' . PHP_EOL, Console::FG_GREEN);
        return ExitCode::OK;
    }

}
