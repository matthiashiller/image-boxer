/**
 * Image Boxer plugin for Craft CMS
 *
 * ImageBoxerUtility Utility JS
 *
 * @author    Matthias Hiller
 * @copyright Copyright (c) 2021 Matthias Hiller
 * @link      https://hiller.digital
 * @package   ImageBoxer
 * @since     1.0.0
 */
