<?php
/**
 * Image Boxer plugin for Craft CMS 3.x
 *
 * Resize and optimise  images
 *
 * @link      https://hiller.digital
 * @copyright Copyright (c) 2021 Matthias Hiller
 */

namespace hillerdigital\imageboxer\assetbundles\imageboxerutilityutility;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * ImageBoxerUtilityUtilityAsset AssetBundle
 *
 * AssetBundle represents a collection of asset files, such as CSS, JS, images.
 *
 * Each asset bundle has a unique name that globally identifies it among all asset bundles used in an application.
 * The name is the [fully qualified class name](http://php.net/manual/en/language.namespaces.rules.php)
 * of the class representing it.
 *
 * An asset bundle can depend on other asset bundles. When registering an asset bundle
 * with a view, all its dependent asset bundles will be automatically registered.
 *
 * http://www.yiiframework.com/doc-2.0/guide-structure-assets.html
 *
 * @author    Matthias Hiller
 * @package   ImageBoxer
 * @since     1.0.0
 */
class ImageBoxerUtilityUtilityAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * Initializes the bundle.
     */
    public function init()
    {
        // define the path that your publishable resources live
        $this->sourcePath = "@hillerdigital/imageboxer/assetbundles/imageboxerutilityutility/dist";

        // define the dependencies
        $this->depends = [
            CpAsset::class,
        ];

        // define the relative path to CSS/JS files that should be registered with the page
        // when this asset bundle is registered
        $this->js = [
            'js/ImageBoxerUtility.js',
        ];

        $this->css = [
            'css/ImageBoxerUtility.css',
        ];

        parent::init();
    }
}
