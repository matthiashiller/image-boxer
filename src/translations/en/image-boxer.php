<?php
/**
 * Image Boxer plugin for Craft CMS 3.x
 *
 * Resize and optimise  images
 *
 * @link      https://hiller.digital
 * @copyright Copyright (c) 2021 Matthias Hiller
 */

/**
 * Image Boxer en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('image-boxer', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Matthias Hiller
 * @package   ImageBoxer
 * @since     1.0.0
 */
return [
    'Image Boxer plugin loaded' => 'Image Boxer plugin loaded',
];
