<?php

namespace hillerdigital\imageboxer\transformers;

use craft\helpers\FileHelper;
use hillerdigital\imageboxer\exceptions\ImageBoxerException;
use hillerdigital\imageboxer\ImageBoxer;
use hillerdigital\imageboxer\models\TransformModel;

use Craft;
use hillerdigital\imageboxer\optimizers\JpegoptimOptimizer;
use hillerdigital\imageboxer\optimizers\PngquantOptimizer;
use hillerdigital\imageboxer\traits\RunShellCommandTrait;
use Imagick;
use yii\base\Exception;

class Transformer
{
    use RunShellCommandTrait;

    /**
     * @var string
     */
    public static $tempPath;

    /**
     * @param TransformModel $transform
     * @param bool           $consoleRequest
     *
     * @return bool|string[]
     * @throws ImageBoxerException
     */
    public static function transformForSite(TransformModel $transform, $consoleRequest = false)
    {
        $transform->setImageInstance();
        $imagick = self::_setTransforms($transform);
        $imagickClone = clone $imagick;
        $imagickClone->setImageFormat('webp');

        $targetPath = self::_setTargetPath($transform);
        $targetFile = $targetPath.DIRECTORY_SEPARATOR.$transform->filename;

        $imagick->writeImage($targetFile.'.'.$transform->ext);
        $imagickClone->writeImage($targetFile.'.webp');

        if ($consoleRequest) {
            return true;
        }

        return [
            'origin' => $targetFile.'.'.$transform->ext,
            'webp' => $targetFile.'.webp',
        ];

    }


    /**
     * @param TransformModel $transform
     *
     * @throws ImageBoxerException
     */
    public static function transform(TransformModel $transform): void
    {
        $transform->setImageInstance();
        self::$tempPath = Craft::$app->path->getTempPath();

        $formatted = null;
        if ($transform->ext != $transform->extOrigin) {
            $formatted = self::_setFormat($transform);
        }
/*
        if ($transform->imageInstance->getImageFormat() == 'GIF' && $transform->imageInstance->getNumberImages() > 1) {
            return AnimatedGifTransformer::transform($transform);
        }
*/

        if ($formatted) {
            FileHelper::unlink($formatted);
        }
        $hqImage = self::_generateHQImage($transform);
        self::_convertImage($hqImage, $transform);
    }

    /**
     * @param TransformModel $transform
     *
     * @return string
     * @throws ImageBoxerException
     */
    private static function _setFormat(TransformModel $transform): string
    {
        $file = self::$tempPath.DIRECTORY_SEPARATOR.'temp_'.$transform->filename.'.'.$transform->ext;
        try {
            $imager = new Imagick($transform->originFile);
            $imager->setImageFormat('png');
            $imager->writeImage($file);
            $transform->setImageInstance($file);
            return $file;
        } catch (\ImagickException $e) {
            Craft::error($e->getMessage(), __METHOD__);
            throw new ImageBoxerException($e->getMessage(), $e->getCode(), $e);
        }
    }


    /**
     * @param TransformModel $transform
     *
     * @return string
     */
    private static function _generateHQImage(TransformModel $transform): string
    {
        $tempImage = self::$tempPath.DIRECTORY_SEPARATOR.$transform->filename.'.'.$transform->ext;

        $imagick = $transform->imageInstance;

        $imagick->setImageCompression(Imagick::COMPRESSION_NO);
        $imagick->setImageCompressionQuality(100);
        $imagick->setOption('png:compression-level', 9);

        $imagick = self::_setTransforms($transform, 0.75, $imagick);
        $imagick->writeImage($tempImage);

        return $tempImage;
    }

    /**
     * @param TransformModel $transform
     * @param int            $blur
     * @param Imagick|null   $imagick
     *
     * @return Imagick|null
     */
    private static function _setTransforms(TransformModel $transform, $blur = 1, Imagick $imagick = null): Imagick
    {
        $imagick = $imagick ?? $transform->imageInstance;
        $rawWidth = $imagick->getImageWidth();
        $rawHeight = $imagick->getImageHeight();
        $filter = Imagick::FILTER_LANCZOS;

        switch ($transform->mode) {
            case 'fit':
                if ($rawWidth > $rawHeight) {
                    $imagick->resizeImage($transform->width, null, $filter, $blur);
                } else {
                    $imagick->resizeImage(null, $transform->height, $filter, $blur);
                }
                break;
            case 'stretch':
                $imagick->resizeImage($transform->width, $transform->height, $filter, $blur);
                break;
            case "letterbox":
                $imagick->setImageBackgroundColor('#'.$transform->bgColor);
                $imagick->thumbnailImage($transform->width, $transform->height, true, true);
                break;
            case "crop":
                $posLeft = floor($rawWidth * $transform->fpX) - floor($transform->width / 2);
                $posTop = floor($rawHeight * $transform->fpY) - floor($transform->height / 2);
                $posLeft = min(max($posLeft, 0), $rawWidth - $transform->width);
                $posTop = min(max($posTop, 0), $rawHeight - $transform->height);
                $imagick->cropImage($transform->width, $transform->height, $posLeft, $posTop);
                break;
            default:
                self::_fitImage($imagick, $transform);
                $ratio = $imagick->getImageWidth() / $imagick->getImageHeight();
                if ($transform->ratio > $ratio) {
                    $posTop = self::_getPosition($imagick->getImageHeight(), $transform->height, $transform->fpY);
                    $imagick->cropImage($transform->width, $transform->height, 0, $posTop);
                } else {
                    $posLeft = self::_getPosition($imagick->getImageWidth(), $transform->width, $transform->fpX);
                    $imagick->cropImage($transform->width, $transform->height, $posLeft, 0);
                }
        }

        return $imagick;
    }

    /**
     * @param Imagick        $imagick
     * @param TransformModel $transform
     */
    private static function _fitImage(Imagick $imagick, TransformModel $transform): void
    {
        $filter = Imagick::FILTER_LANCZOS;
        $blur = 0.75;
        $aspect = $imagick->getImageWidth() / $imagick->getImageHeight();

        if ($aspect < $transform->ratio) {
            $imagick->resizeImage($transform->width, null, $filter, $blur);
        } else {
            $imagick->resizeImage(null, $transform->height, $filter, $blur);
        }
    }

    /**
     * @param $sourceDim
     * @param $targetDim
     * @param $position
     *
     * @return int
     */
    private static function _getPosition($sourceDim, $targetDim, $position): int
    {
        $pos = floor($sourceDim * $position) - floor($targetDim / 2);
        return min(max($pos, 0), $sourceDim - $targetDim);
    }

    /**
     * @param string         $file
     * @param TransformModel $transform
     *
     * @return string[]
     * @throws \Exception
     */
    private static function _convertImage(string $file, TransformModel $transform): array
    {

        $targetPath = self::_setTargetPath($transform);
        $targetFile = $targetPath.DIRECTORY_SEPARATOR.$transform->filename;

        $settings = ImageBoxer::$plugin->getSettings();

        $cmd = $settings->cwebpPath;
        $cmd .= ' -q '.$transform->quality;
        $cmd .= ' -pass 10 -m 6 -mt';
        $cmd .= ' '.$file;
        $cmd .= ' -o '.$targetFile.'.webp';

        $result = self::runShellCommand($cmd);
        Craft::info('Command "'.$cmd.'" returned "'.$result.'"');

        switch ($transform->ext) {
            case 'jpg':
                JpegoptimOptimizer::optimize($file, $targetPath, $transform);
                break;
            case 'gif':

            default:
                PngquantOptimizer::optimize($file, $targetPath, $transform);
        }
        FileHelper::unlink($file);

        return [
            'origin' => $targetFile.'.'.$transform->ext,
            'webp' => $targetFile.'.webp',
        ];
    }

    /**
     * @param TransformModel $transform
     *
     * @return string
     * @throws \Exception
     */
    private static function _setTargetPath(TransformModel $transform): string
    {

        $path = $transform->getTargetPath();
        try {
            FileHelper::createDirectory($path);
            return $path;
        } catch (Exception $e) {
            throw new \RuntimeException($e->getMessage());
        }
    }


}
