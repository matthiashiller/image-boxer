<?php


namespace hillerdigital\imageboxer\transformers;


use hillerdigital\imageboxer\models\TransformModel;

class AnimatedGifTransformer
{
    public static function transform(TransformModel $transform)
    {

    }

    private static function _generateHQImage(TransformModel $transform): string
    {
        $tempImage = self::$tempPath.DIRECTORY_SEPARATOR.$transform->filename.'.'.$transform->ext;

        $imagick = $transform->imageInstance;

        $format = $imagick->getImageFormat();
        $numbers = $imagick->getNumberImages();

        $rawWidth = $imagick->getImageWidth();
        $rawHeight = $imagick->getImageHeight();
        $filter = Imagick::FILTER_LANCZOS;
        $blur = 0.75;

        $posLeft = floor($rawWidth * $transform->fpX) - floor($transform->width / 2);
        $posLeft = min(max($posLeft, 0), $rawWidth - $transform->width);
        $posTop = floor($rawHeight * $transform->fpY) - floor($transform->height / 2);
        $posTop = min(max($posTop, 0), $rawHeight - $transform->height);

        $imagick = $imagick->coalesceImages();

        switch ($transform->mode) {
            case 'fit':

                if ($transform->ratio > 1) {
                    do {
                        $imagick->resizeImage($transform->width, null, Imagick::FILTER_BOX, 1);
                    } while ($imagick->nextImage());

                } else {
                    do {
                        $imagick->resizeImage(null, $transform->height, Imagick::FILTER_BOX, 1);
                    } while ($imagick->nextImage());
                }
                break;
            case 'stretch':
                $imagick->resizeImage($transform->width, $transform->height, $filter, $blur);
                break;
            case "letterbox":
                $imagick->setImageBackgroundColor('#'.$transform->bgColor);
                $imagick->thumbnailImage($transform->width, $transform->height, true, true);
                break;
            case "crop":
                $imagick->cropImage($transform->width, $transform->height, $posLeft, $posTop);
                break;
            default:
        }
        $imagick = $imagick->deconstructImages();
        $imagick->writeImages($tempImage, true);

        return $tempImage;
    }
}
