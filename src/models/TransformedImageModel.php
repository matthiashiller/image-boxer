<?php


namespace hillerdigital\imageboxer\models;


use Craft;
use craft\base\Model;
use craft\helpers\FileHelper;
use yii\base\InvalidConfigException;

class TransformedImageModel extends Model
{
    /**
     * @var string
     */
    public $path;

    /**
     * @var string
     */
    public $filename;
    /**
     * @var string
     */
    public $imgTitle;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $extension;

    /**
     * @var string
     */
    public $mimeType;

    /**
     * @var int
     */
    public $width;

    /**
     * @var int
     */
    public $height;

    /**
     * @var int|float
     */
    public $size;

    /**
     * @var bool
     */
    public $isNew;

    public function __construct(TransformModel $transformModel, $isNew = true, $config = [])
    {
        parent::__construct($config);

        $this->imgTitle = $transformModel->imgTitle;
        $request = Craft::$app->getRequest();
        if ($request->accepts('image/webp')) {
            $current = $transformModel->targetFiles['webp'];
            $this->extension = 'webp';
        } else {
            $current = $transformModel->targetFiles['origin'];
            $this->extension = $transformModel->ext;
        }

        $this->path = $current;
        $url = str_replace(Craft::getAlias('@webroot'), '', $current);
        $this->url = '/'.ltrim($url, '/');

        $this->filename = $transformModel->filename;

        try {
            $this->mimeType = FileHelper::getMimeType($current);
        } catch (InvalidConfigException $e) {
            // just ignore
        }

        $imageInfo = @getimagesize($current);
        if (\is_array($imageInfo) && $imageInfo[0] !== '' && $imageInfo[1] !== '') {
            $this->width = $imageInfo[0];
            $this->height = $imageInfo[1];
        } else {
            $this->width = $transformModel->width;
            $this->height = $transformModel->height;
        }

        $this->size = @filesize($current);
        $this->isNew = $isNew;
    }

    public function __toString(): string
    {
        return (string)$this->url;
    }


}
