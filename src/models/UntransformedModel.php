<?php
namespace hillerdigital\imageboxer\models;

use craft\base\Model;
use Craft;
use craft\helpers\FileHelper;

class UntransformedModel extends Model
{

    public string $path;
    public string $filename = 'placeholder';
    public string $imgTitle = 'Bild nicht gefunden';
    public string $url = '/_processed/placeholder.svg';
    public string $extension = 'svg';
    public string $mimeType = 'image/svg+xml';
    public int $width = 600;
    public int $height = 450;
    public bool $isNew = false;

    /**
     * @var int|float
     */
    public $size;

    /**
     * @throws \yii\base\Exception
     */
    public function __construct($transforms, $config = [])
    {
        parent::__construct($config);

        $publicDir = Craft::getAlias('@webroot/_processed');
        $image = Craft::getAlias('@hillerdigital/imageboxer/web/images/placeholder.svg');
        $copy = $publicDir.DIRECTORY_SEPARATOR.'placeholder.svg';

        $this->path = $copy;
        $this->size = @filesize($image);

        if (is_string($transforms)) {
            $transformsConfig = Craft::$app->config->getConfigFromFile('image-boxer-transforms');
            $transforms = $transformsConfig[$transforms];
            if (isset($transforms['transforms'])) {
                $transforms = $transforms['transforms'][0];
            }
        }
        if (isset($transforms['width'])) {
            $this->width = $transforms['width'];
            $this->height = (int)($transforms['width'] / 4 * 3);
        } else if (isset($transforms['height']) ){
            $this->height = $transforms['height'];
            $this->width = (int)($transforms['height'] / 3 * 4);
        }

        if(!is_dir('@webroot/_processed')) {
            FileHelper::createDirectory($publicDir);
        }
        if (!file_exists($copy)) {
            copy($image, $copy);
        }
    }
}
