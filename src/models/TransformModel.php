<?php


namespace hillerdigital\imageboxer\models;


use Craft;
use craft\base\Model;
use craft\elements\Asset;
use craft\helpers\Assets;
use craft\helpers\FileHelper;

use hillerdigital\imageboxer\exceptions\ImageBoxerException;
use Imagick;
use yii\base\InvalidConfigException;

class TransformModel extends Model
{

    /**
     * @var null|Imagick
     */
    public $imageInstance = null;
    /**
     * @var string
     */
    public $originFile;

    /**
     * @var null|Asset
     */
    public $asset = null;

    /**
     * @var string
     */
    public $filename;

    /**
     * @var string
     */
    public $imgTitle;

    /**
     * @var string
     */
    public $ext;

    /**
     * @var string
     */
    public $extOrigin;

    /**
     * @var bool
     */
    public $issetWidth = false;

    /**
     * @var int
     */
    public $width;

    /**
     * @var bool
     */
    public $issetHeight = false;

    /**
     * @var int
     */
    public $height;

    /**
     * @var float
     */
    public $ratio;

    /**
     * @var int
     */
    public $quality;

    /**
     * @var string
     */
    public $mode = '';

    /**
     * @var float
     */
    public $fpX = 0.5;

    /**
     * @var float
     */
    public $fpY = 0.5;

    /**
     * @var float
     */
    public $cropZoom = 1.0;

    /**
     * @var string
     */
    public $bgColor = 'ffffff';

    /**
     * @var string
     */
    public $targetPath;

    /**
     * @var array
     */
    public $targetFiles;

    /**
     * @var bool;
     */
    public $fileExists;

    /**
     * @var string
     */
    private $_processedPath;

    /**
     * @var string
     */
    private $_eval;


    /**
     * TransformModel constructor.
     *
     * @param       $sourceImage
     * @param array $transforms
     * @param array $config
     *
     * @throws ImageBoxerException
     * @throws InvalidConfigException
     */
    public function __construct($sourceImage, array $transforms, $config = [])
    {
        parent::__construct($config);

        $this->_processedPath = rtrim(Craft::getAlias('@webroot'), '/').DIRECTORY_SEPARATOR.'_processed'.DIRECTORY_SEPARATOR;

        if ($sourceImage instanceof Asset) {
            $this->asset = $sourceImage;
            $this->imgTitle = $sourceImage->title;
            $this->originFile = $sourceImage->getImageTransformSourcePath();
            $this->_eval = (string)$sourceImage->id;
        } else {
            $origin = Craft::getAlias('@webroot').'/'.ltrim($sourceImage, '/');
            $this->originFile = $origin;
            $this->imgTitle = Assets::filename2Title(pathinfo($this->originFile)['filename']);
            $this->_eval = ltrim($sourceImage, '/');
        }

        if (isset($transforms['format'])) {
            $this->ext = $transforms['format'];
        } else {
            $this->_setFileExtension();
        }
        $this->_setTransformSettings($transforms);
        $this->_setFilename();
        $this->_setTargetFiles();
    }


    // Public Methods
    // =========================================================================

    /**
     * @param string|null $image
     *
     * @throws ImageBoxerException
     */
    public function setImageInstance(string $image = null): void
    {
        try {
            $this->imageInstance = new Imagick($image ?? $this->originFile);
        } catch (\ImagickException $e) {
            Craft::error($e->getMessage(), __METHOD__);
            throw new ImageBoxerException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     *
     */
    public function unsetImageInstance(): void
    {
        $this->imageInstance->destroy();
        $this->imageInstance = null;
    }

    /**
     * @return string
     */
    public function getTargetPath(): string
    {
        if ($this->asset) {
            return $this->_processedPath.$this->asset->id;
        }
        return $this->_processedPath.pathinfo($this->_eval)['dirname'];
    }

    // Private Methods
    // =========================================================================
    /**
     * @throws InvalidConfigException
     */
    private function _setFileExtension(): void
    {
        $mime = FileHelper::getMimeType($this->originFile);
        $this->extOrigin = FileHelper::getExtensionByMimeType($mime);
        switch ($mime) {
            case 'image/jpeg':
                $ext = 'jpg';
                break;
            case 'image/gif':
                $ext = 'gif';
                break;
            default:
                $ext = 'png';
        }
        $this->ext = $ext;
    }

    /**
     * @param $transforms
     */
    private function _setTransformSettings($transforms): void
    {
        $sizes = getimagesize($this->originFile);
        $width = $sizes[0];
        $height = $sizes[1];

        if (!isset($transforms['ratio'])) {
            $this->ratio = $width / $height;
        } else {
            $this->ratio = $transforms['ratio'];
        }

        if (!isset($transforms['width']) && !isset($transforms['height'])) {
            $this->width = $width;
            $this->height = $height;
        } else if (isset($transforms['width'], $transforms['height'])) {
            $this->width = $transforms['width'];
            $this->height = $transforms['height'];
            $this->issetWidth = true;
            $this->issetHeight = true;
            // overwrite ratio if width and height are set
            $this->ratio = $transforms['width'] / $transforms['height'];
        } else if (isset($transforms['width']) && !isset($transforms['height'])) {
            $this->issetWidth = true;
            $this->width = $transforms['width'];
            $this->height = (int)((1 / $this->ratio) * $transforms['width']);
        } else {
            $this->issetHeight = true;
            $this->height = $transforms['height'];
            $this->width = $this->ratio * $transforms['height'];
        }

        if (isset($transforms['quality'])) {
            $this->quality = $transforms['quality'];
        } else {
            $this->quality = Craft::$app->getConfig()->getGeneral()->defaultImageQuality;
        }

        if (isset($transforms['mode'])) {
            $this->mode = $transforms['mode'];
        }

        if (isset($transforms['bgColor'])) {
            $this->bgColor = ltrim($transforms['bgColor'], '#');
        }

        if (isset($transforms['position'])) {
            $pos = $transforms['position'];
            if (is_string($pos) && $pos == 'focalPoint' && $this->asset) {
                $fp = $this->asset->getFocalPoint();
                $this->fpX = $fp['x'];
                $this->fpY = $fp['y'];
            } else {
                if (isset($pos['x'])) {
                    $x = $pos['x'];
                    if ($x < 0) {
                        $x = 0;
                    }
                    if ($x > 1) {
                        $x = 1;
                    }
                    $this->fpX = $x;
                }
                if (isset($pos['y'])) {
                    $y = $pos['y'];
                    if ($y < 0) {
                        $y = 0;
                    }
                    if ($y > 1) {
                        $y = 1;
                    }
                    $this->fpY = $y;
                }
            }

        }

        if (isset($transforms['cropZoom'])) {
            $this->cropZoom = $transforms['cropZoom'];
        }
    }

    private function _setFilename(): void
    {
        $filename = FileHelper::sanitizeFilename(pathinfo($this->originFile)['filename'], ['asciiOnly' => true]);
        $toHash = [
            'ext: '.$this->ext,
            'width: '.$this->width,
            'height: '.$this->height,
            'ratio: '.$this->ratio,
            'quality: '.$this->quality,
            'mode: '.$this->mode,
            'fpX: '.$this->fpX,
            'fpY: '.$this->fpY,
            'cropZoom: '.$this->cropZoom,
            'bgColor: '.$this->bgColor
        ];
        $this->filename = $filename.'-'.md5($this->_eval.implode('-', $toHash));
    }

    private function _setTargetFiles(): void
    {
        $targetPath = $this->_processedPath;
        if ($this->asset) {
            $targetPath .= $this->asset->id.DIRECTORY_SEPARATOR;
        } else {
            $targetPath .= pathinfo($this->_eval)['dirname'].DIRECTORY_SEPARATOR;
        }
        $file = $targetPath.$this->filename;
        $this->targetFiles['origin'] = $file.'.'.$this->ext;
        $this->targetFiles['webp'] = $file.'.webp';

        $request = Craft::$app->getRequest();
        if ($request->isConsoleRequest) {
            $this->fileExists = file_exists($this->targetFiles['webp']) && file_exists($this->targetFiles['origin']);
        } else {
            if ($request->accepts('image/webp')) {
                $this->fileExists = file_exists($this->targetFiles['webp']);
            } else {
                $this->fileExists = file_exists($this->targetFiles['origin']);
            }
        }

    }

}
