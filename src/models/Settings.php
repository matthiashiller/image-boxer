<?php


namespace hillerdigital\imageboxer\models;


use craft\base\Model;

class Settings extends Model
{
    /**
     * @var string
     */
    public $cwebpPath = '/usr/bin/cwebp';

    /**
     * @var string
     */
    public $cwebpOptions;

    /**
     * @var string
     */
    public $jpegoptimPath = '/usr/bin/jpegoptim';

    /**
     * @var string
     */
    public $jpegoptimOptions;

    /**
     * @var string
     */
    public $optipngPath = '/usr/bin/optipng';

    /**
     * @var string
     */
    public $optipngOptions;

    /**
     * @var string
     */
    public $pngquantPath = '/usr/bin/pngquant';

    /**
     * @var string
     */
    public $pngquantOptions;

    /**
     * @var array
     */
    public $transforms = [];

    /**
     * @var array
     */
    public $tasks = [];

}
