<?php

namespace hillerdigital\imageboxer\optimizers;

use Craft;
use hillerdigital\imageboxer\ImageBoxer;
use hillerdigital\imageboxer\models\TransformModel;
use hillerdigital\imageboxer\traits\RunShellCommandTrait;

class JpegoptimOptimizer
{
    use RunShellCommandTrait;

    /**
     * @param                $file
     * @param                $targetPath
     * @param TransformModel $transform
     */
    public static function optimize($file, $targetPath, TransformModel $transform): void
    {
        $settings = ImageBoxer::$plugin->getSettings();

        $cmd = $settings->jpegoptimPath;
        $cmd .= ' ';
        $cmd .= '-s -m '.$transform->quality;
        $cmd .= ' ';
        $cmd .= '-o -d '.$targetPath;
        $cmd .= ' ';
        $cmd .= $file;

        $result = self::runShellCommand($cmd);
        Craft::info('Command "'.$cmd.'" returned "' . $result . '"');

    }
}
