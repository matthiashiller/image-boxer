<?php


namespace hillerdigital\imageboxer\optimizers;


use Craft;
use hillerdigital\imageboxer\ImageBoxer;
use hillerdigital\imageboxer\models\TransformModel;
use hillerdigital\imageboxer\traits\RunShellCommandTrait;

class PngquantOptimizer
{
    use RunShellCommandTrait;

    /**
     * @param                $file
     * @param                $targetPath
     * @param TransformModel $transform
     */
    public static function optimize($file, $targetPath, TransformModel $transform): void
    {
        $settings = ImageBoxer::$plugin->getSettings();

        $cmd = $settings->pngquantPath;
        $cmd .= ' ';
        $cmd .= '-f --ext .png';
        $cmd .= ' ';
//        $cmd .= '--speed';
//        $cmd .= ' ';
//        $cmd .= '1';
//        $cmd .= ' ';
        $cmd .= '--quality';
        $cmd .= ' ';
        $cmd .= '20-'.$transform->quality;
        $cmd .= ' ';
        $cmd .= $targetPath.DIRECTORY_SEPARATOR.$transform->filename.'.png';
        $cmd .= ' ';
        $cmd .= $file;

        $result = self::runShellCommand($cmd);
        Craft::info('Command "'.$cmd.'" returned "' . $result . '"');
    }
}
