<?php


namespace hillerdigital\imageboxer\optimizers;


use Craft;
use hillerdigital\imageboxer\ImageBoxer;
use hillerdigital\imageboxer\models\TransformModel;
use hillerdigital\imageboxer\traits\RunShellCommandTrait;

class OptipngOptimizer
{
    use RunShellCommandTrait;

    /**
     * @param                $file
     * @param                $targetPath
     * @param TransformModel $transform
     */
    public static function optimize($file, $targetPath, TransformModel $transform): void
    {
        $settings = ImageBoxer::$plugin->getSettings();

        $cmd = $settings->optipngPath;
        $cmd .= ' ';
        $cmd .= '-o7';
        $cmd .= ' ';
        $cmd .= '-f4';
        $cmd .= ' ';
        $cmd .= '-strip';
        $cmd .= ' ';
        $cmd .= 'all';
        $cmd .= ' ';
        $cmd .= '-out';
        $cmd .= ' ';
        $cmd .= $targetPath.DIRECTORY_SEPARATOR.$transform->filename.'.png';
        $cmd .= ' ';
        $cmd .= $file;

        $result = self::runShellCommand($cmd);
        Craft::info('Command "'.$cmd.'" returned "' . $result . '"');
    }
}
