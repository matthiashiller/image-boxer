<?php
/**
 * Image Boxer plugin for Craft CMS 3.x
 *
 * Resize and optimise  images
 *
 * @link      https://hiller.digital
 * @copyright Copyright (c) 2021 Matthias Hiller
 */

namespace hillerdigital\imageboxer\variables;

use craft\elements\Asset;
use hillerdigital\imageboxer\exceptions\ImageBoxerException;
use hillerdigital\imageboxer\ImageBoxer;

use Craft;
use hillerdigital\imageboxer\models\TransformedImageModel;
use yii\base\InvalidConfigException;

/**
 * Image Boxer Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.imageBoxer }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Matthias Hiller
 * @package   ImageBoxer
 * @since     1.0.0
 */
class ImageBoxerVariable
{
    // Public Methods
    // =========================================================================

    /**
     * @param       $image
     * @param string|array $transforms
     * @param null  $options
     *
     * @return array|TransformedImageModel|object
     * @throws ImageBoxerException
     * @throws InvalidConfigException
     */
    public function transform($image, $transforms, $options = null)
    {
        return ImageBoxer::$plugin->imageBoxer->transform($image, $transforms, $options);
    }

    /**
     * @param $images
     *
     * @return string
     */
    public function srcset($images): string
    {
        $sources = [];
        if (!\is_array($images)) {
            $images = [$images];
        }

        foreach ($images as $image) {
            $sources[] = $image->url.' '.$image->width.'w';
        }
        return implode(', ', $sources);
    }

    /**
     * @param null $config
     *
     * @return string
     */
    public function placeholder($config = null): string
    {
        $width = $config['width'] ?? 1;
        $height = $config['height'] ?? 1;
        $color = $config['color'] ?? 'transparent';
        return 'data:image/svg+xml;charset=utf-8,' . rawurlencode("<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height' style='background:$color'/>");
    }

    /**
     * @return array
     * @throws \hillerdigital\imageboxer\exceptions\ImageBoxerException
     * @throws \yii\base\InvalidConfigException
     */
    public function generate(): array
    {
        return ImageBoxer::$plugin->imageBoxer->generators();
    }

}
