<?php
/**
 * Image Boxer plugin for Craft CMS 3.x
 *
 * Resize and optimise  images
 *
 * @link      https://hiller.digital
 * @copyright Copyright (c) 2021 Matthias Hiller
 */

namespace hillerdigital\imageboxer\services;


use craft\elements\Asset;
use craft\helpers\ArrayHelper;

use Craft;
use craft\base\Component;
use hillerdigital\imageboxer\exceptions\ImageBoxerException;
use hillerdigital\imageboxer\jobs\OptimizeImage;
use hillerdigital\imageboxer\models\TransformedImageModel;
use hillerdigital\imageboxer\models\UntransformedModel;
use hillerdigital\imageboxer\models\TransformModel;

use hillerdigital\imageboxer\transformers\Transformer;

/**
 * ImageBoxerService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Matthias Hiller
 * @package   ImageBoxer
 * @since     1.0.0
 */
class ImageBoxerService extends Component
{

    // Public Methods
    // =========================================================================

    /**
     * @param Asset|array|string $image
     * @param                    $transforms
     * @param                    $options
     *
     * @return array|TransformedImageModel|UntransformedModel
     * @throws ImageBoxerException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function transform($image, $transforms, $options = null)
    {
        if ($image instanceof craft\elements\db\AssetQuery) {
            $image = $image->one();
        }

        if (!$image || (is_array($image) && !isset($image[0]))) {
            return new UntransformedModel($transforms);
        }

        if (is_array($image)) {
            $image = $image[0];
        }

        $transformModels = $this->_setTransformModels($image, $transforms, $options);
        $transformedImages = [];
        foreach ($transformModels as $transformModel) {
            /* @var TransformModel $transformModel */
            if ($transformModel->fileExists) {
                $transformedImages[] = new TransformedImageModel($transformModel, false);
            } else {
                $transformed = Transformer::transformForSite($transformModel);
                $transformModel->targetFiles = $transformed;
                $transformedImages[] = new TransformedImageModel($transformModel);

                $transformModel->unsetImageInstance();
                $queue = Craft::$app->getQueue();
                $queue->push(new OptimizeImage([
                    'model' => $transformModel,
                ]));
            }
        }

        return count($transformedImages) > 1 ? $transformedImages : $transformedImages[0];
    }

    /**
     * @param $transformModel
     *
     * @throws ImageBoxerException
     */
    public function optimizeTransformedImages($transformModel): void
    {
        Transformer::transform($transformModel);
    }


    /**
     * @param null $task
     *
     * @return array
     * @throws ImageBoxerException
     * @throws \yii\base\InvalidConfigException
     */
    public function generators($task = null): array
    {
        $generatorSetting = Craft::$app->config->getConfigFromFile('image-boxer-generate');
        $data = null;
        $toTransform = [];

        if (isset($generatorSetting['elements'])) {
            foreach ($generatorSetting['elements'] as $element) {
                $query = $element['elementType']::find();
                foreach ($element['criteria'] as $key => $val) {
                    $query->$key($val);
                }
                $images = [];
                foreach ($query->all() as $item) {
                    if (isset($element['one']) && $element['one']) {
                        $images[] = $item[$element['field']][0];
                    } else {
                        foreach ($item[$element['field']] as $img) {
                            $images[] = $img;
                        }
                    }
                }
                foreach ($images as $image) {
                    $transformModels = [];
                    if (is_array($element['transforms'])) {
                        foreach ($element['transforms'] as $item) {
                            $transformModels = ArrayHelper::merge($transformModels, $this->_setTransformModels($image, $item));
                        }
                    } else {
                        $transformModels = $this->_setTransformModels($image, $element['transforms']);
                    }

                    foreach ($transformModels as $transform) {
                        /* @var TransformModel $transform */
                        if (!$transform->fileExists) {
                            $toTransform[$transform->filename] = $transform;
                        }
                    }
                }
            }

        }

        return $toTransform;
    }

    /**
     * @param $image
     * @param $transforms
     * @param $options
     *
     * @return array
     * @throws ImageBoxerException
     * @throws \yii\base\InvalidConfigException
     */
    private  function _setTransformModels($image, $transforms, $options = null): array
    {
        $transformModels = [];
        if (is_string($transforms)) {
            $transformsConfig = Craft::$app->config->getConfigFromFile('image-boxer-transforms');
            $transforms = $transformsConfig[$transforms];

            if (!isset($transforms['transforms'])) {
                $transforms['transforms'] = [$transforms];
            }

            foreach ($transforms['transforms'] as $value) {
                if (isset($transforms['default']) && count($transforms['default']) > 0) {
                    $value = ArrayHelper::merge($transforms['default'], $value);
                }
                $transformModels[] = new TransformModel($image, $value);
            }
        } else {
            if (!isset($transforms[1])) {
                $transforms = [$transforms];
            }
            foreach ($transforms as $value) {
                if ($options) {
                    $value = ArrayHelper::merge($options, $value[0] ?? $value);
                }
                $transformModels[] = new TransformModel($image, $value);
            }
        }

        return $transformModels;
    }

}
